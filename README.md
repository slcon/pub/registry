# Public Package Registry

This project's [Package Registry] is public and and does not require authentication.  
The project's ID [(**34256364**)][this project] identifies the registry when installing packages.

## npm

Use the [Project-level endpoint][gitlab endpoint] to install packages using npm.   

Set the global registry for the package scope using [`npm config`]:
<pre>
npm config set <b>@slcpub</b>:registry <b>https://gitlab.com/api/v4/projects/34256364/packages/npm/</b>
</pre>
or with an [`.npmrc` file]:
<pre>
<b>@slcpub</b>:registry=<b>https://gitlab.com/api/v4/projects/34256364/packages/npm/</b>
</pre>

## References

  * [Package Registry](https://docs.gitlab.com/ee/user/packages/package_registry/)
  * [Store all packages in one project](https://docs.gitlab.com/ee/user/packages/workflows/project_registry.html) 

[this project]:     https://gitlab.com/softlife-consulting/packages
[package registry]: https://gitlab.com/slcon/pub/registry/-/packages
[gitlab endpoint]:  https://docs.gitlab.com/ee/user/packages/npm_registry/#use-the-gitlab-endpoint-for-npm-packages
[`npm config`]:     https://docs.gitlab.com/ee/user/packages/npm_registry/#project-level-npm-endpoint
[`.npmrc` file]:    https://docs.gitlab.com/ee/user/packages/npm_registry/#project-level-npm-endpoint-1
